//index.js
//获取应用实例
const app = getApp()
var value1 = '0';
var value2;
var value3 = '0';
var value4 = '0';
var value5 = '0';
var value6;
var value7;
var value8;
var value9;
var value_title;
var limit_num = 0;

Page({
  data: {

    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    switch1Checked: false,
    switch2Checked: false,
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }


    setInterval(function () {
      this.setData({
        motto1: value1,
        motto_id: value2,
        motto_title: value3,
        motto_description: value4,
        motto_online: value5,
        motto_device_image: value6,
        motto_lat: value7,
        motto_lng: value8,
        motto_onlinetime: value9,

      })
      this.get_access_token();
      this.send_message();
      this.get_device_message();
      this.get_inputs_message();
      
    }.bind(this), 1000);
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },

  //获取授权码(access_token)
  get_access_token: function () {
    wx.request({
      url: 'https://www.bigiot.net/oauth/token?grant_type=password&client_id=510&client_secret=0e8402e9ad&username=6599&password=b94b323e6b',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      method: "POST",
      success: function (res) {
        console.log(res.data);
        value1 = res.data.access_token;
        console.log(value1);
      },
    })
  },
  // 获取设备信息
  get_device_message: function () {
    wx.request({
      url: 'https://www.bigiot.net/oauth/dev',
      header: {
        "Content-Type": "application/json"
      },
      data: {
        access_token: value1,//授权凭证,凭证内部已包含用户ID信息
        id:9532,
      },
      method: "GET",
      success: function (res) {
        console.log(res.data);
        value2 = res.data.id;
        value3 = res.data.title;
        value4 = res.data.description;
        value5 = res.data.online;
        value6 = res.data.image;
        value7 = res.data.lat;
        value8 = res.data.lng;
        value9 = res.data.online_time;
    
      },
    })
  },
  //获取接口信息
  get_inputs_message: function () {
    wx.request({
      url: 'https://www.bigiot.net/oauth/myinputs',
      header: {
        "Content-Type": "application/json"
      },
      data: {
        access_token: value1,//授权凭证,凭证内部已包含用户ID信息
      },
      method: "GET",
      success: function (res) {
        console.log(res.data);
        value_title = res.data[0].title;
        console.log(value_title);
      },
    })
  },
 send_message:function(){
   wx.request({
     url: 'https://www.bigiot.net/oauth/say',
     header: {
       "Content-Type": "application/json"
     },
     data:{
       access_token: value1,
       id: "D9532",
       c: "play",
       sign: "开",
     },
     method: "POST",
     success:function(res){
       console.log(res.data);
     }
   })
 }




})
